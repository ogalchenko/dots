#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls -al --color=auto'
PATH=$PATH:~/.src/scripts
PS1='[\u@\h \W]\$ '
