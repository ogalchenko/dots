#!/bin/bash

run () {
    if ! pgrep -f $1 ;
    then
	$@&
    else 
	kill $(pgrep -f $1)
    fi
}

run setxkbmap -layout us,ru,ua -option grp:alt_shift_toggle -option caps:ctrl_modifier
run feh --bg-scale ~/Pictures/.wallpapers/wall.jpg
run aslstatus
run picom --config ~/.config/picom/picom.conf
run dunst
run xrdb ~/.Xresources
