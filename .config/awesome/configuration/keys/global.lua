local awful = require("awful")
local gears = require("gears")

local hotkeys_popup = require("awful.hotkeys_popup")
local brightness_widget = require("widgets.bar.brightness.brightness")


local scratch = require("modules.scratch")

local globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({modkey}, "Return", function ()
                                    if client.focus == awful.client.getmaster() then
                                       awful.client.swap.byidx(1)
                                       awful.client.focus.byidx(-1)
                                    else
                                       awful.client.setmaster(client.focus)
                                    end
                              end),
    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey, "Shift"   }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),
    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
       {description = "select previous", group = "layout"}),
    awful.key({ modkey }, "`", function() scratch.toggle("urxvt -name scratch", {instance="scratch"}) end),

    -- Set layouts keys
    -- tile
    awful.key({ modkey,  }, "t", function () awful.layout.set(awful.layout.suit.tile)   end,
       {description = "set tile layout", group = "layout"}),
    -- tile left
    awful.key({ modkey,  "Shift" }, "t", function () awful.layout.set(awful.layout.suit.tile.left)   end,
       {description = "set tile left layout", group = "layout"}),

    -- floating
    awful.key({ modkey,  }, "f", function () awful.layout.set(awful.layout.suit.floating)   end,
       {description = "set floating layout", group = "layout"}),
    -- centered master (magnifier)
    awful.key({ modkey,  }, "c", function () awful.layout.set(awful.layout.suit.magnifier)   end,
       {description = "set magnifire layout", group = "layout"}),
    -- bottom stack
    awful.key({ modkey,  }, "b", function () awful.layout.set(awful.layout.suit.tile.bottom)  end,
       {description = "set tile bottom layout", group = "layout"}),
    -- H bottom stack
    awful.key({ modkey, "Shift" }, "b", function () awful.layout.set(awful.layout.suit.tile.top)   end,
       {description = "set tile top layout", group = "layout"}),
    -- monocle
    awful.key({ modkey,        }, "m", function () awful.layout.set(awful.layout.suit.max)   end,
       {description = "set max layout", group = "layout"}),
    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "p",
    		function ()
	        	awful.util.spawn("rofi -show run")
		end,
              {description = "run rofi prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Fn keys
    -- Volume mute
    awful.key({ }, "#121", function () awful.util.spawn("amixer set Master toggle") end),
    -- Volume down
    awful.key({ }, "#122", function () awful.util.spawn("amixer set Master 5%-") end),
    -- Volume up
    awful.key({ }, "#123", function () awful.util.spawn("amixer set Master 5%+") end),
    -- Mic mute
    awful.key({ }, "#198", function () awful.util.spawn("amixer -set Capture toggle") end),
    -- Brightness down
    awful.key({ }, "#232", function () brightness_widget:dec() end, {description = "decrease brightness", group = "custom"}),
    -- Brightness up
    awful.key({ }, "#233", function () brightness_widget:inc() end, {description = "increase brightness", group = "custom"}))

-- Layouts
--    awful.key({ modkey }, "t", function () awful.layout.suit.max end))
    -- Switch video output (rofi dmenu)
    --awful.key({ }, "#235", function () awful.util.spawn("amixer -D pulse sset Master 5%-") end),
    -- Air mode
    --awful.key({ }, "#246", function () awful.util.spawn("amixer -D pulse sset Master 5%-") end),
    -- Tools
    --awful.key({ }, "#179", function () awful.util.spawn("amixer -D pulse sset Master 5%-") end),
    -- Bluetooth On/Off
    --awful.key({ }, "#245", function () awful.util.spawn("amixer -D pulse sset Master 5%-") end),
    -- Touchpad on/Off
    -- awful.key({ }, "#122", function () awful.util.spawn("amixer -D pulse sset Master 5%-") end),
    -- Favorites menu
    --awful.key({ }, "#164", function () awful.util.spawn("amixer -D pulse sset Master 5%-") end),

return globalkeys
