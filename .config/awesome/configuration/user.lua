local _M = {
	terminal = "urxvt",
	editor   = "vim",
	modkey   = "Mod4",
	useless_gaps = 2,
}

return _M
