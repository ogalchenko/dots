local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local client = client

local taglist = function (s)
   -- Each screen has its own tag table.
   awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

   local taglist_buttons = gears.table.join(
      awful.button({ }, 1, function(t) t:view_only() end),
      awful.button({ modkey }, 1, function(t)
            if client.focus then
               client.focus:move_to_tag(t)
            end
      end),
      awful.button({ }, 3, awful.tag.viewtoggle),
      awful.button({ modkey }, 3, function(t)
            if client.focus then
               client.focus:toggle_tag(t)
            end
      end),
      awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
      awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
   )

   local tags =  awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons,
           widget_template = {
            {
                {
                     {
                        id    = 'text_role',
                        align = 'center',
                        forced_height = dpi(24),
                        forced_width = dpi(24),
                        widget = wibox.widget.textbox
                    },
                    {
                        text = '',
                        widget = wibox.widget.textbox
                    },
                    layout = wibox.layout.fixed.vertical,
                },
                widget = wibox.container.margin
            },
            id     = 'background_role',
            widget = wibox.container.background,
        }
   }

   return wibox.widget {
      {
         tags,
         widget = wibox.container.background,
      },
      left   = dpi(2),
      right  = dpi(2),
      top    = dpi(2),
      bottom = dpi(2),
      widget = wibox.container.margin,
   }

end

return taglist
