local awful   = require("awful")
local fs      = require("gears.filesystem")
local naughty = require("naughty")

local startrup_apps = {
	"setxkbmap -layout us,ru,ua -option grp:alt_shift_toggle -option caps:ctrl_modifier",
	"feh --bg-scale ~/Pictures/.wallpapers/wall.jpg",
	"picom --config ~/.config/picom/picom.conf",
	"xrdb ~/.Xresources",
}

local spawn_once = function(cmd)
	local findme = cmd
	local firstspace = cmd:find(" ")
	if firststapce then
		findme = cmd:sub(0, firstspace - 1)
	end
    
	awful.spawn.with_shell(
	string.format('pgrep -u $USER -x %s > /dev/null || (%s)', findme, cmd),
	function(_, stderr)
		if stderr or stderr ~= '' then
           naughty.notify({
              preset = naughty.config.presets.critical,
              title = "Error starting application",
              text = "Error while starting " .. cmd,
              timeout = 10,})
		end
	end
	)
end

for _, app in ipairs(startrup_apps) do
	spawn_once(app)
end


