﻿;; -*- flycheck-disabled-checkers: (emacs-lisp-checkdoc); lexical-binding: t -*-
;; Local Variables:
;; byte-compile-warnings: (not obsolete free-vars unresolved noruntime lexical make-local)
;; End:

(eval-when-compile
  (when (eq system-type 'windows-nt)
    (defvar org-mode-directory "f:/Dropbox/Org/")
    (defvar pyworkon-venvs-folder "f:/Development/projects/python/vEnvs")
    (defvar projectile-project-folder '("f:/Development/projects/python" "f:/Development/projects/rust")))

  (when (eq system-type 'gnu/linux)
    (defvar org-mode-directory "~/org/")
    (defvar pyworkon-venvs-folder "~/Development/projects/python/vEnvs")
    (defvar projectile-project-folder '("~/Development/projects/python" "~/Development/projects/rust"))))

(defvar bootstrap-version)
(setq straight-check-for-modifications nil)
(setq vc-follow-symlinks t)
(setq straight-use-package-by-default t)
(setq straight-vc-git-default-clone-depth 1)

(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(use-package use-package-core
  :straight (:type built-in)
  :custom
  (use-package-verbose nil)
  (use-package-compute-statistics nil)
  (use-package-always-defer t)
  (use-package-expand-minimally t)
  (use-package-enable-imenu-support t))

(use-package use-package-ensure-system-package)

(use-package use-package-chords
  :demand
  :config (key-chord-mode 1))

(use-package use-package-hydra
  :functions
  hydra-default-pre
  hydra-keyboard-quit
  hydra--call-interactively-remap-maybe
  hydra-show-hint
  hydra-set-transient-map
  :demand t)

(use-package emacs
  :straight (:type built-in)
  :bind (("C-h"     . 'backward-delete-char-untabify)
         ("M-f"     . 'forward-to-word)
         ("M-b"     . 'backward-to-word)
         ("C-x k"   . 'kill-current-buffer)
         ("C-x M-w" . 'yank-whole-buffer)
         ("C-<tab>" . 'other-frame)
         ("C-<f5>"  . display-line-numbers-mode)
         ("C-c d"   . 'local/duplicate-start-of-line-or-region)
         ("C-c I"   . 'local/open-init-el)
         ("C-c <tab>"   . 'local/indent-buffer)
         ([remap dabbrev-expand] . 'hippie-expand))
  :bind-keymap(([F1]   .  help-map))
  :hook((before-save . delete-trailing-whitespaces)
        (emacs-startup . (lambda ()
                           (let ((startup-time (float-time (time-subtract after-init-time before-init-time))))
                             (message "Emacs ready in %.2f seconds with %d garbage collections." startup-time gcs-done)))))                       
  :custom
  (load-prefer-newer t)
  (package-user-dir "~/.emacs.d/elpa")
  (nsm-settings-file "~/.emacs.d/network-security.data")
  (package--init-file-ensured t)

  (buffer-file-coding-system 'utf-8)
  (save-buffer-coding-system 'utf-8)

  (cursor-in-non-selected-windows 'hollow)
  
  (history-delete-duplicates t)
  (history-length 300)
  (help-window-select t)          
  
  (inhibit-startup-message t)
  (inhibit-splash-screen t)
  
  (sentence-end-double-space nil)     
  (select-enable-clipboard t)       
  
  (indent-tabs-mode nil)            
  (tab-always-indent 'complete)

  (resize-mini-windows t)
  (tab-width 4)
  
  (show-trailing-whitespace nil)      
  (ring-bell-function 'ignore)
  (indicate-empty-lines t)
  (backward-delete-char-untabify-method 'hungry)

  (cursor-type 'box)
  (blink-cursor-mode t)
  (show-paren-mode t)

  :config

  (prefer-coding-system 'utf-8) 
  (set-default-coding-systems 'utf-8)
  
  (if (eq system-type 'windows-nt)
    (progn
      (set-clipboard-coding-system 'utf-16-le)
      (set-selection-coding-system 'utf-16-le)))
  
  (cd "~/")   
 
  (defalias #'yes-or-no-p #'y-or-n-p)
  
  (delete-selection-mode 1)

  (global-prettify-symbols-mode nil)

  (column-number-mode   1)
  (size-indication-mode 1)
  (global-hl-line-mode  1)
  (electric-pair-mode   1)
  (set-fringe-mode      10)
  (put 'narrow-to-region 'disabled nil)
  (setq frame-title-format '("" "%b %& Emacs " emacs-version))

  (global-auto-revert-mode t)
  
  (save-place-mode)
  
  (setq-default mode-line-format nil) ;; Do not show default modeline until simple-modeline is loaded
  
  (defun local/reload-emacs-configuration ()
    (interactive)
    (load-file "~/.emacs.d/init.el"))

  (defun local/indent-buffer ()
    "Autoindent the whole buffer"
    (interactive)
    (indent-region (point-min) (point-max)))

  (defun local/duplicate-start-of-line-or-region ()
    (interactive)
    (if mark-active
        (local/duplicate-region)
      (local/duplicate-start-of-line)))

  (defun local/duplicate-start-of-line ()
    (let ((text (buffer-substring (point)
                                  (beginning-of-thing 'line))))
      (forward-line)
      (push-mark)
      (insert text)
      (open-line 1)))

  (defun local/duplicate-region ()
    (let* ((end (region-end))
           (text (buffer-substring (region-beginning)
                                   end)))
      (goto-char end)
      (insert text)
      (push-mark end)
      (setq deactivate-mark nil)
      (exchange-point-and-mark)))
  
  (defun yank-whole-buffer ()
    "Copy whole buffer to killring."
    (interactive)
    (kill-ring-save (point-min)(point-max)))

  (defun local/open-init-el ()
    "Open the init file."
    (interactive)
    (find-file user-init-file)))

(use-package diminish)

(use-package tool-bar
  :straight (:type built-in)
  :config
  (tool-bar-mode -1))

(use-package scroll-bar
  :straight (:type built-in)
  :config
  (scroll-bar-mode -1))

(use-package menu-bar
  :straight (:type built-in)
  :config
  (menu-bar-mode -1)
  :bind
  ([S-f10] . menu-bar-mode))

(use-package tooltip
  :straight (:type built-in)
  :defer t
  :custom
  (tooltip-mode -1))

(use-package files
  :straight (:type built-in)
  :custom
  (create-lockfiles nil)
  (make-backup-files nil)
  (auto-save-default nil)
  
  (backup-directory-alist '((".*" . "~/.emacs.d/backups/")))
  (auto-save-file-name-transforms '((".*" "~/.emacs.d/backups/" t))))

(use-package so-long
  :straight (:type built-in)
  :config
  (global-so-long-mode t)
  :custom
  (so-long-threshold 400))

(use-package recentf
  :straight (:type built-in)
  :custom
  (recentf-auto-cleanup 50)
  (recent-save-file "~/.emacs.d/recentf")  
  :config
  (recentf-mode)
  (run-with-idle-timer 60 t 'recentf-save-list))

(use-package custom
  :straight (:type built-in)
  :no-require t
  :config
  (setq custom-file (expand-file-name "~/.emacs.d/custom.el" user-emacs-directory))
  (when (file-exists-p custom-file)
    (load custom-file)))

(use-package subword
  :straight (:type built-in)
  :diminish
  :config
  (subword-mode t))

(use-package superword
  :straight (:type built-in)
  :diminish
  :config
  (superword-mode t))

(use-package eldoc
  :straight (:type built-in)
  :diminish)

(use-package ffap 
  :straight (:type built-in)
  :bind(:map ctl-x-map
             ("F" . find-file-at-point)))

(use-package winner-mode
  :straight (:type built-in)
  :hook(after-init . winner-mode)
  :bind(("C-c ," . winner-undo)
        ("C-c ." . winner-redo)))

(use-package isearch
  :straight (:type built-in)
  :custom
  (search-whitespace-regexp ".*")
  (isearch-lax-whitespace t)
  (isearch-rexexp-lax-whitespace t))

(use-package window
  :straight (:type built-in)
  :bind(("M-o"  . 'other-window)
        ("M-0"  . 'delete-window)))

(use-package time
  :straight (:type built-in)
  :hook(after-init . display-time-mode)
  :custom
  (display-time-format "%l:%M %p %b %y")
  (display-time-default-load-average nil)
  (display-time-24hr-format t))

(use-package gcmh
  :diminish gcmh-mode
  :custom
  (gcmh-idle-delay 3)
  :init (gcmh-mode t))
 
(use-package async
  :after bytecomp
  :hook ((after-init . async-bytecomp-package-mode)
     (dired-mode . dired-async-mode)))

(use-package hydra)

(use-package which-key
  :diminish
  :hook(after-init . which-key-mode)
  :custom
  (which-key-sort-order #'which-key-prefix-then-key-order)
  (which-key-sort-uppercase-first nil)
  (which-key-add-column-padding 1)
  (which-key-idle-delay 1.0))

(use-package page-break-lines
  :diminish
  :hook(after-init . global-page-break-lines-mode))

(use-package reverse-im
  :demand t
  :custom
  (reverse-im-input-methods '("russian-computer"))
  :config
  (reverse-im-mode t))

(use-package ivy
  :diminish
  :hook(after-init . ivy-mode)
  :preface
  (defun my/copy-full-path (x)
      (kill-new x))
  :custom
  (ivy-use-virtual-buffers nil)
  (ivy-height 10)
  (ivy-virtual-abbreviate 'full)
  (ivy-count-format "[%d/%d] ")
  (ivy-extra-directories nil)
  (ivy-fixed-height-minibuffer t)
  (enable-recursive-minibuffers t)

  :bind(:map ivy-minibuffer-map
             ("C-o" . hydra-ivy/body))
  :config
  (setq ivy-re-builders-alist '((swiper-isearch . ivy--regex-plus)
                                (t              . ivy--regex-fuzzy)))
  (setq ivy-initial-inputs-alist nil)

  (ivy-add-actions 'counsel-find-file
                   '(("n" my/copy-full-path "copy full path")))

  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))

(use-package ivy-rich
  :hook(ivy-mode . ivy-rich-mode)
  :config
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))

(use-package ivy-hydra)

(use-package flx
  :after ivy counsel)

(use-package counsel
  :diminish
  :hook(ivy-mode    . counsel-mode)
  :bind(("C-x C-f"  . counsel-find-file)
        ("M-y"      . counsel-yank-pop)
        ("M-x"      . counsel-M-x)
        ("C-x C-b"  . ibuffer)
        ("M-<f10>"  . counsel-tmm)
        ([remap describe-bindings] . counsel-descbinds)
        ("C-c o c"  . counsel-org-capture)
        :map help-map
        ("f"        . counsel-describe-function)
        ("v"        . counsel-describe-variable)
        ("l"        . counsel-find-library)
        ("a"        . counsel-apropos)
        ("F"        . counsel-describe-face)
        :map global-map
        :prefix-map user-custom-map
        :prefix "C-;"
        ("s a"      . counsel-ag)
        ("s r"      . counsel-rg)
        ("s g"      . counsel-grep)
        ("s G"      . counsel-git-grep)
        ("b"        . counsel-bookmark)
        ("B"        . counsel-switch-buffer-other-window)
        ("r"        . counsel-recentf)
        ("d"        . counsel-dired-jump)
        ("C-d"      . dired-jump)
        ("f"        . counsel-file-jump)
        ("j"        . counsel-find-symbol)
        ("l"        . counsel-locate)
        ("p"        . counsel-package)
        ("h"        . counsel-command-history)
        ("H"        . counsel-minibuffer-history)
        ("C"        . cua-selection-mode)
        :map company-active-map
        ("M-s"      . counsel-company)))

(use-package counsel-projectile
  :after counsel ivy
  :hook(counsel-mode . counsel-projectile-mode))

(use-package swiper
  :custom
  (swiper-action-recenter t)
  (swiper-include-line-number-in-search t)
  :bind(([remap isearch-backward] . swiper-isearch-backward)
        ([remap isearch-forward]  . swiper-isearch)
        ("C-c C-."                . swiper-thing-at-point)
        ("C-M-s"                  . swiper-all)
        :map swiper-map
        ("M-s" . swiper-isearch-toggle)))

(use-package projectile
  :bind-keymap ("C-c p" . projectile-command-map)
  :defer
  :commands
  projectile-switch-project-by-name
  projectile-find-file
  :custom
  (projectile-project-root-files-functions
   '(projectile-root-local
     projectile-root-top-down
     projectile-root-bottom-up
     projectile-root-top-down-recurring))
  (projectile-enable-caching t)
  (projectile-completion-system 'ivy)
  (projectile-sort-order 'recentf)
  (projectile-project-search-path projectile-project-folder)
  (projectile-globally-ignored-file-suffixes '("#" "~" ".swp" ".o" ".so" ".exe" ".dll" ".elc" ".pyc" ".jar" "*.class"))
  (projectile-globally-ignored-directories '(".git" "node_modules" "__pycache__" ".vs" ".mypy_cache" "debug"))
  (projectile-globally-ignored-files '("TAGS" "tags" ".DS_Store" "GTAGS"))
  (projectile-mode-line-prefix "")
  (projectile-tags-command "ctags -R -e --languages=python --fields=afiklmnst --file-scope=yes --format=2"))

(use-package company
  :diminish
  :bind(:map company-mode-map
             ([remap indent-for-tab-command] . company-indent-or-complete-common))
  :hook (after-init . global-company-mode)
  :functions
  company-select-next
  company-select-previous
  company-complete-common-or-cycle
  :custom
  (company-minimum-prefix-length 2)
  (company-auto-complete nil)
  (company-idle-delay 0.5)

  (company-require-match nil)
  (company-tooltip-limit 15)
  (company-tooltip-align-annotations t)
  (company-tooltip-maximum-width 45)
  (company-tooltip-margin 1)
  
  (company-dabbrev-downcase nil)
  (company-dabbrev-other-buffers t)
  (company-dabbrev-code-other-buffers t)
  (company-dabbrev-ignore-case t)
  (company-complete-number t)
  (company-show-numbers t)

  (company-transformers '(company-sort-by-backend-importance))
  (company-global-modes '(not ;org-mode
                              eshell-mode
                              Info-mode
                              help-mode
                              Custom-mode
                              epa-key-list-mode
                              shell-mode))

  :bind(:map company-active-map
             ("C-n" . 'company-select-next)
             ("C-p" . 'company-select-previous)
             ([tab] . 'company-complete-common-or-cycle)
             ("C-j" . 'company-complete)
             ([?\r] . 'company-complete)
             ("M-o" . company-other-backend)
             ("M-h" . company-show-doc-buffer)
             ("M-w" . company-show-location)
             ("C-w" . backward-kill-word)
             ("C-h" . backward-delete-char-untabify)))

(use-package company-web
  :after web-mode
  :config
  (add-to-list 'company-backends '(company-web-html)))

(use-package transpose-frame
  :after counsel
  :bind(:map user-custom-map
             :prefix-map transpose-frame-cusom-map
             :prefix "t"
             ("t" . transpose-frame)
             ("i" . flip-frame)
             ("o" . flop-frame)
             ("r" . rotate-frame)
             ("c" . rotate-frame-clockwise)
             ("a" . rotate-frame-anticlockwise)))

(use-package ssh)

(use-package vlf
  :after ivy counsel
  :init
  (ivy-add-actions 'counsel-find-file '(("l" vlf "view large file"))))

(use-package wgrep)

(use-package good-scroll
  :straight (:host github :repo "io12/good-scroll.el")
  :hook(after-init . good-scroll-mode)
  :commands good-scroll-mode
  :custom
  (good-scroll-duration 0.2)
  (good-scroll-point-jump 4))

(use-package imenu
  :straight (:type built-in)
  :custom
  (imenu-auto-rescan t))

(use-package imenu-list
  :after counsel
  :bind(:map user-custom-map
             :prefix-map imenu-list-custom-map
             :prefix "i"
             ("i"  . counsel-imenu)
             ("l"  . imenu-list)
             ("t"  . imenu-list-smart-toggle)))

(use-package ibuffer-projectile
  :hook(ibuffer . (lambda ()
      (ibuffer-projectile-set-filter-groups)
      (unless (eq ibuffer-sorting-mode 'alphabetic)
        (ibuffer-do-sort-by-alphabetic))))
  :config
    (setq ibuffer-formats
      '((mark modified read-only " "
              (name 18 18 :left :elide)
              " "
              (size 9 -1 :right)
              " "
              (mode 16 16 :left :elide)
              " "
              project-relative-file))))

(use-package ibuffer-sidebar
  :bind("C-x B" . 'ibuffer-sidebar-toggle-sidebar))

(use-package move-text
  :bind(("M-[" . move-text-up)
        ("M-]" . move-text-down)))

(use-package multiple-cursors
  :after hydra
  :bind("C-c m" . hydra-multicursor/body)
  :hydra(hydra-multicursor(:hint nil :color pink)
                                "
   ^Down  ^                    ^Up   ^                        ^Buffer^               
  ─^──────^────────────────────^─────^────────────────────────^──────^────────────────────── 
   [_n_] region like this      [_p_] prev region line this    [_L_] each line in region          
   [_w_] word line this        [_W_] prev word like this      [_B_] start of each line 
   [_s_] symbol like this      [_S_] prev symbol like this    [_E_] end of each line           
   [_u_] unmark line this      [_U_] unmark prev like this    [_A_] all in buffer like this           
   [_t_] skip to next          [_T_] skip to prev             [_x_] search in region
                                                          [_q_] cancel"
                                ;; Down
                                ("n" mc/mark-next-like-this)
                                ("w" mc/mark-next-like-this-word)
                                ("s" mc/mark-next-like-this-symbol)
                                ("u" mc/unmark-next-like-this)
                                ("t" mc/skip-to-next-like-this)
                                ;; Up
                                ("p" mc/mark-previous-like-this)
                                ("W" mc/mark-previous-like-this-word)
                                ("S" mc/mark-previous-like-this-symbol)
                                ("U" mc/unmark-previous-like-this)
                                ("T" mc/skip-to-previous-like-this)
                                ;; Buffer
                                ("L" mc/edit-lines :color blue)
                                ("B" mc/edit-beginnings-of-lines :color blue)
                                ("E" mc/edit-ends-of-lines :color blue)
                                ("A" mc/mark-all-like-this :color blue)
                                ("x" mc/mark-all-in-region :color blue)
                                ("q" nil :color blue)))

(use-package iedit
  :bind(("C-." . iedit-mode)))

(use-package embrace
  :bind (("C-," . 'embrace-commander)))

(use-package avy
  :chords(("jw" . avy-goto-char-timer)
          ("jl" . avy-goto-line))
  :custom
  (avy-timeout-seconds 0.8))

(use-package goto-chg
  :chords(("jc"  .  goto-last-change)
          ("jr"  .  goto-last-change-reverse)))

(use-package goto-line-preview
  :commands goto-line-preview
  :bind
  ([remap goto-line] . goto-line-preview))

(use-package expand-region
  :after hydra
  :functions
  er/mark-inside-pairs
  er/mark-outside-pairs
  er/mark-paragraph
  er/mark-word
  er/mark-inside-quotes
  er/mark-outside-quotes
  :bind(("C-'"   . er/expand-region)
        ("M-'"   . er/contract-region)
        ("C-M-'" . hydra-expand-region/body))
  :hydra(hydra-expand-region (:hint nil :color blue)
                           "
   ^Language^                  ^Pairs^                        ^Misc^               
  ─^────────^──────────────────^─────^────────────────────────^────^────────────────────── 
   [_w_] word                  [_p_] inside pairs             [_u_] mark URL
   [_s_] symbol                [_P_] outside pairs            [_e_] mark Email
   [_m_] method call           [_q_] inside quotes    
   [_c_] comment               [_Q_] ouside quotes     
   [_d_] defun 
                                                          [_g_] cancel" 

                           ;; Language
                           ("w" er/mark-word)
                           ("s" er/mark-symbol)
                           ("m" er/mark-method-call)
                           ("c" er/mark-comment)
                           ("d" er/mark-defun)
                           ;; Pairs
                           ("p" er/mark-inside-pairs)
                           ("P" er/mark-outside-pairs)
                           ("q" er/mark-inside-quotes)
                           ("Q" er/mark-outside-quotes)
                           ;; Misc
                           ("u" er/mark-url)
                           ("e" er/mark-email)
                           ("g" nil)))

(use-package dired
  :straight (:type built-in)
  :custom
  (dired-listing-switches "-alh")
  :config
  (put 'dired-find-alternate-file 'disabled nil))

(use-package dired-x
  :straight (:type built-in)
  :bind
  ([remap list-directory] . dired-jump)
  (:map ctl-x-4-map ("d" . dired-jump-other-window))
  :custom
  (dired-bind-jump nil))

(use-package dired+
  :after dired
  :straight (dired+ :type git :host github :repo "emacsmirror/dired-plus"))

(use-package diredfl
  :hook(dired-mode . diredfl-mode))

(use-package dired-hide-dotfiles
  :bind
  (:map dired-mode-map
        ("." . dired-hide-dotfiles-mode))
  :hook (dired-mode . dired-hide-dotfiles-mode))

(use-package dired-hacks-utils
  :bind(:map dired-mode-map
               ([tab]     . 'dired-subtree-toggle)
               ([backtab] . 'dired-subtree-cycle)))

(use-package dired-narrow
  :bind
  (:map dired-mode-map
        ("/" . dired-narrow))
  :hook(dired-mode . dired-narrow-mode)) ;; Do a mapping here for this function

(use-package peep-dired
  :after dired
  :bind(:map dired-mode-map ("P" . peep-dired)))

(use-package dired-sidebar
  :bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
  :custom
  (dired-sidebar-subtree-line-prefix "__")
  (dired-sidebar-width 38)
  (dired-sidebar-theme 'nerd))

(use-package dired-rsync
  :bind (:map dired-mode-map ("r" . dired-rsync)))

(use-package lsp-mode
  :commands lsp
  :bind-keymap("C-c l" . lsp-command-map)
  :hook
  ((python-mode rustic-mode csharp-mode lua-mode powershell-mode) . lsp)
  (lsp . 'lsp-enable-which-key-integration)
  :custom
  (lsp-lens-enable nil)
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-modeline-code-actions-enable t)
  (lsp-completion-show-detail t)
  (lsp-completion-show-kind t)
  (lsp-log-io nil)
  (lsp-prefer-flymake nil)
  (lsp-restart 'auto-restart)
  (lsp-enable-snippet nil)
  (lsp-enable-xref t)
  (lsp-keep-workspace-alive t)
  (lsp-enable-on-type-formatting t)
  (lsp-imenu-show-container-name t)
  (lsp-enable-file-watchers t)
  (lsp-eldoc-render-all nil)
  (lsp-eldoc-enable-hover nil)
  (lsp-signature-render-all nil)
  (lsp-enable-folding nil)
  (lsp-enable-symbol-highlighting nil)
  (lsp-enable-imenu t)
  (lsp-idle-delay 0.500)
  (lsp-flycheck-live-reporting t)
  (lsp-completion-provider :capf)
  (read-process-output-max (* 1024 1024)))

(use-package lsp-ui 
  :diminish
  :hook (lsp-mode . lsp-ui-mode)
  :bind (:map lsp-command-map
              ("e" . lsp-ui-flycheck-list)
              :map lsp-ui-mode-map
              ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
              ([remap xref-find-references] . lsp-ui-peek-find-references))
  :custom
  ; lsp sideline
  (lsp-ui-sideline nil)
  (lsp-ui-sideline-show-diagnostics nil)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-show-code-actions nil)
  (lsp-ui-sideline-update-mode nil)
  (lsp-ui-sideliine-delay nil)
  ; lsp peek 
  (lsp-ui-peek-enable t)
  (lsp-ui-peek-show-directory t)
  ; lsp doc
  (lsp-ui-doc-enable t)
  (lsp-ui-doc-position 'bottom)
  (lsp-ui-doc-delay 5))


(use-package dap-mode
  :diminish
  :hook((lsp-mode     . dap-mode))
  :bind(("<f12>"      . dap-debug)
        ("<f8>"       . dap-continue)
        ("<f9>"       . dap-next)
        ("<M-f11>"    . dap-step-in)
        ("C-M-<f11>"  . dap-step-out)
        ("<f7>"       . dap-breakpoint-toggle))
  :custom
  (dap-print-io t)
  :config
  (require 'dap-lldb)
  (require 'dap-gdb-lldb)
  ;; installs .extension/vscode
  (dap-gdb-lldb-setup)
  (dap-register-debug-template
   "Rust::LLDB Run Configuration"
   (list :type "lldb"
         :request "launch"
         :name "LLDB::Run"
   :gdbpath "rust-lldb"
         :target nil
         :cwd nil))
  
  (dap-mode 1)
  (dap-ui-mode 1)
  (dap-ui-controls-mode -1))

(use-package dap-python
  :straight nil
  :demand t
  :after (dap-mode python))

(use-package dap-lldb
  :straight nil
  :demand t
  :after(dap-mode rustic-mode))

(use-package lsp-ivy
  :after(lsp-mode))

(use-package flycheck
  :hook (after-init . global-flycheck-mode)
  :init (setq flycheck-mode-line "F.")
  :diminish
  :custom
  (flycheck-global-modes '(not lisp-interaction-mode eshell shell)))

(use-package flycheck-indicator
  :hook (global-flycheck-mode . flycheck-indicator-mode))

(use-package highlight-numbers
  :hook (prog-mode . highlight-numbers-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package yasnippet
  :diminish yas-minor-mode
  :hook (prog-mode . yas-global-mode)
  :bind
  (:map yas-minor-mode-map
        ([(tab)] . nil)
        ("TAB"   . nil)
        ("<tab>" . nil)
        ("C-j"   . yas-expand))
  (:map yas-keymap
        ([(shift tab)] . nil)
        ([backtab]     . nil)
        ("S-TAB"       . nil)
        ("S-<tab>"     . nil)
        ([(tab)]       . nil)
        ("TAB"         . nil)
        ("<tab>"       . nil)
        ("C-S-z"       . yas-prev-field)
        ("C-z"         . yas-next-field-or-maybe-expand)))

(use-package yasnippet-snippets
  :after yasnippet)

(use-package magit
  :bind(("C-c s" . magit-status)))

(use-package diff-hl
  :hook((prog-mode vc-dir-mode) . turn-on-diff-hl-mode))

(use-package sphinx-doc
  :hook(python-mode . sphinx-doc-mode)
  :diminish)

(use-package pyvenv
  :diminish
  :hook(python-mode . pyvenv-mode)
  :init
  (message "Setting workon directory")
  (setenv "WORKON_HOME" pyworkon-venvs-folder)
  :custom
  (pyvenv-mode-line-indicator '(pyvenv-virtual-env-name ("[venv:" pyvenv-virtual-env-name "] "))))

(use-package python-docstring
  :hook ((python-mode . python-docstring-mode))
  :diminish)

(use-package rustic
  :custom
  (rustic-lsp-server 'rust-analyzer)
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :config
  (add-hook 'before-save-hook 'rustic-format-buffer))

(use-package toml-mode)

(use-package impatient-mode
  :diminish)

(use-package restclient
  :diminish "REST"
  :mode ("\\.http\\'" . restclient-mode)
  :custom
  (restclient-log-request nil))

(use-package htmlize
  :commands htmlize-buffer)

(use-package web-mode
  :diminish "Web"
  :mode ("\\.html?\\'" "\\.css\\'" "\\.json\\'" "\\.xml'")
  :init (add-hook 'web-mode-hook
                  (lambda ()
                    (set (make-local-variable 'company-backends) '(company-web-html company-abbrev))))
  :custom
  (add-to-list 'company-dabbrev-code-modes 'web-mode)
  (web-mode-markup-indent-offset 3)
  (web-mode-css-indent-offset 3)
  (web-mode-code-indent-offset 3)
  (web-mode-code-indent-offset 3)
  (web-mode-enable-auto-pairing t)
  (wed-mode-enable-css-colorization t)
  (web-mode-enable-current-element-highlight t))

(use-package emmet-mode
  :hook ((html-mode sgml-mode mhtml-mode css-mode web-mode) . emmet-mode)
  :diminish
  :custom
  (emmet-move-cursor-between-quotes t)
  (emmet-indentation 2))

(use-package cider)

(use-package lua-mode)

(use-package yaml-mode
  :mode "\\.yaml\\'"
  :hook
  (yaml-mode . highlight-indent-guides-mode)
  (yaml-mode . display-line-numbers-mode))

(use-package jenkins)

(use-package jenkinsfile-mode)

(use-package powershell)

(use-package csharp-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-tree-sitter-mode)))

(straight-register-package 'org)
(straight-register-package 'org-contrib)

(use-package org
  :straight (:type built-in)
  :bind(("C-c o a" . org-agenda)
        ("C-c o s" . org-store-link)
        ("C-c o r" . counsel-org-folder)
        ("C-c o u" . update-org-agenda-files-list))
  :functions update-org-agenda-files-list
  :preface
  (defun counsel-org-folder ()
    "Use counsel-rg to search for the current directory"
    (interactive)
    (if (eq (file-remote-p default-directory) nil)
        (counsel-rg "" org-mode-directory)
      (call-interactively #'rgrep)))
  
  (defun update-org-agenda-files-list ()
    "Update org-agenda-files if some new appears."
    (interactive)
    (setq org-agenda-files (mapcar (lambda (file)
                                   (cons file '(:maxlevel . 4)))
                                   (directory-files-recursively org-mode-directory "\\.org$")))
    
    (setq org-refile-targets org-agenda-files))

  :config
  (setq org-ellipsis "⤵"
        org-refile-use-outline-path 'file
        org-directory org-mode-directory
        org-outline-path-complete-in-steps nil
        org-hide-leading-stars t)
  
  (update-org-agenda-files-list)
  (add-hook 'before-save-hook 'update-org-agenda-files-list))

(use-package org-capture
  :straight (:type built-in)
  :custom
  (org-reverse-note-order t)
  (org-capture-templates
   '(("t" "Todo" entry
    (file+headline (lambda () (concat org-directory "journal.org")) "Todo")
    "* TODO %?\n Added:%U\n %i\n" :prepend t)
   
   ("i" "Idea" entry
    (file+headline (lambda () (concat org-directory "journal.org")) "Idea")
    "* Idea %?\n Added:%U\n %i\n" :prepend t)
   
   ("n" "Notes" entry
    (file+headline (lambda () (concat org-directory "journal.org")) "Notes")
    "* Note %?\n Added:%U\n %i\n" :prepend t) 
   
   ("l" "Links" entry
    (file+headline (lambda () (concat org-directory "journal.org")) "Links")
    "* %^{Title}\n Visit:%^{Url}\n %?\n Added:%U\n %i\n" :prepend t))))

(use-package ox-pandoc
  :after org-mode)

(use-package org-web-tools
  :after org-mode)

(use-package faces
  :straight nil
  :defer t
  :custom
  (face-font-family-alternatives '(("Fira Code Retina" "Monaco" "Monospace")))
  :custom-face
  (default ((t (:family "Fira Code Retina" :height 115))))
  :config
  (set-fontset-font "fontset-default" 'cyrillic
                    (font-spec :registry "iso10646-1" :script 'cyrillic)))

(use-package doom-themes
  :init(load-theme 'doom-one t)
  :custom
  (doom-themes-enable-bold nil)
  (doom-themes-enable-italic nil))

(use-package simple-modeline
  :hook(after-init . simple-modeline-mode))
